Source: glpk-java
Maintainer: Debian Science Team <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Sébastien Villemot <sebastien@debian.org>
Section: java
Priority: optional
Build-Depends: debhelper-compat (= 13),
               debhelper (>= 12.8~),
               libglpk-dev,
               swig,
               default-jdk
Standards-Version: 4.5.1
Vcs-Browser: https://salsa.debian.org/science-team/glpk-java
Vcs-Git: https://salsa.debian.org/science-team/glpk-java.git
Homepage: http://glpk-java.sourceforge.net
Rules-Requires-Root: no

Package: libglpk-java
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends},
         ${shlibs:Depends}
Description: Java binding to the GNU Linear Programming Kit
 GLPK (GNU Linear Programming Kit) is intended for solving large-scale
 linear programming (LP), mixed integer programming (MIP), and other
 related problems. It is a set of routines written in ANSI C and
 organized in the form of a callable library.
 .
 GLPK supports the GNU MathProg language, which is a subset of the
 AMPL language. GLPK also supports the standard MPS and LP formats.
 .
 This package contains the Java binding to GLPK.
